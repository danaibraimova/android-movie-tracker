package com.dana.movietracker;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.dana.movietracker.Fragments.WaitingListFragment;

/**
 * Created by Dana on 03.12.2017.
 */

// TODO this shit
public class ShowWaitingListListener extends AppCompatActivity
        implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        showFragment(WaitingListFragment.class);
    }

    private void showFragment(Class fragmentClass) {
        android.support.v4.app.Fragment fragment = null;
        try {
            fragment = (android.support.v4.app.Fragment) fragmentClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.flContent, fragment)
                .addToBackStack(null)
                .commit();
    }
}
