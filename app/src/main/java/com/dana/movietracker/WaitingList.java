package com.dana.movietracker;

import com.dana.movietracker.Models.Movie;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dana on 03.12.2017.
 */

public class WaitingList {
    private static final WaitingList listInstance = new WaitingList();

    public List<Movie> moviesList = new ArrayList<Movie>();

    public static WaitingList getInstance() {
        return listInstance;
    }

    private WaitingList() {}
}
