package com.dana.movietracker.Activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dana.movietracker.Models.Movie;
import com.dana.movietracker.R;
import com.dana.movietracker.ShowWaitingListListener;
import com.dana.movietracker.WaitingList;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Dana on 29.11.2017.
 */

public class MovieDetailActivity extends AppCompatActivity {
    Movie movie = null;
    @BindView(R.id.ivMovieBackdrop) ImageView ivMovieBackdrop;
    @BindView(R.id.tvOverview) TextView tvOverview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            movie = (Movie) extras.getSerializable("MOVIE");
            this.setTitle(movie.getTitle());
            tvOverview.setText(movie.getOverview());
            Picasso.with(this)
                    .load(movie.getBackdropPath())
                    .into(ivMovieBackdrop);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(R.mipmap.white_heart);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String snackbarText = "Movie was already added to your waiting list";
                if (!WaitingList.getInstance().moviesList.contains(movie)) {
                    WaitingList.getInstance().moviesList.add(movie);
                    snackbarText = "Movie added to your waiting list";
                }

                Snackbar.make(view, snackbarText, Snackbar.LENGTH_LONG)
                        .setAction("View", new ShowWaitingListListener()).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
