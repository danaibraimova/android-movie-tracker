package com.dana.movietracker.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import com.dana.movietracker.Adapters.MovieRecyclerViewAdapter;
import com.dana.movietracker.Models.Movie;
import com.dana.movietracker.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NowPlayingFragment extends Fragment {

    @BindView(R.id.recycler_view_movies)
    RecyclerView recyclerViewMovies;

    Unbinder unbinder;
    MovieRecyclerViewAdapter adapter;

    private List<Movie> movies;

    public NowPlayingFragment() {
        movies = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_now_playing, container, false);
        unbinder = ButterKnife.bind(this, view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NowPlayingFragment.this.getContext());

        recyclerViewMovies.setHasFixedSize(true);
        recyclerViewMovies.setLayoutManager(linearLayoutManager);

        dataChanged();

        new MyAsync().execute();

        return view;
    }

    private void initializeData() throws Exception {

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://api.themoviedb.org/3/movie/now_playing?api_key=041002b871245f52b51405f0f613270e&language=en-US&page=1")
                .build();

        JSONObject jsonObject = new JSONObject(client.newCall(request).execute().body().string().toString());
        JSONArray jsonArray = jsonObject.getJSONArray("results");
        for(int i = 0; i < jsonArray.length(); ++i) {
            JSONObject object = jsonArray.getJSONObject(i);

            String id = object.getString("id");
            String title = object.getString("title");
            String overview = object.getString("overview");

            float voteAverage = Float.valueOf(object.getString("vote_average"));
            float voteCount = Float.valueOf(object.getString("vote_count"));

            String posterPath = object.getString("poster_path");
            String backdropPath = object.getString("backdrop_path");

            Movie movie = new Movie(id, title, overview,
                    voteAverage, voteCount, posterPath, backdropPath);

            Log.d("Entity_movie", movie.toString());
            movies.add(movie);

            Log.d("Json_object", jsonObject.toString());
        }
    }

    private void dataChanged() {
        Log.d("Movies_list", movies.toString());
        adapter = new MovieRecyclerViewAdapter(getContext(), movies);
        recyclerViewMovies.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private class MyAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            try {
                initializeData();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        // after doInBackground
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("Movies_list_post", movies.toString());
            adapter.notifyDataSetChanged();
        }
    }
}
