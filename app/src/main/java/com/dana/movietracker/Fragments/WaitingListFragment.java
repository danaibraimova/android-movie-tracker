package com.dana.movietracker.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dana.movietracker.Adapters.MovieRecyclerViewAdapter;
import com.dana.movietracker.Models.Movie;
import com.dana.movietracker.R;
import com.dana.movietracker.WaitingList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class WaitingListFragment extends Fragment {
    @BindView(R.id.recycler_view_movies) RecyclerView recyclerViewMovies;

    Unbinder unbinder;

    MovieRecyclerViewAdapter adapter;
    private List<Movie> movies;

    public WaitingListFragment() {
        movies = WaitingList.getInstance().moviesList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_upcoming, container, false);
        unbinder = ButterKnife.bind(this, view);

        LinearLayoutManager llm = new LinearLayoutManager(this.getContext());

        recyclerViewMovies.setHasFixedSize(true);
        recyclerViewMovies.setLayoutManager(llm);

        adapter = new MovieRecyclerViewAdapter(getContext(), movies);
        recyclerViewMovies.setAdapter(adapter);

        return view;
    }

}
